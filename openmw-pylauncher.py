#!/bin/env python

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.


# TODO: use try/catch instead of checking return values

import os
import time
# Warning
# Loading a JSON string from an untrusted source represents a potential security vulnerability. jsonpickle makes no attempt to sanitize the input.
import jsonpickle
import curses
import sys
import textwrap
import numpy
import _thread
import copy
import datetime
import subprocess

class GlobalsManager():
	def __init__(self):
		self.reset()

	def reset(self):
		self.espPathList = []
		self.pureBaseDirList = []

		self.storageManager = StorageManager()
		self.storageManager.reset()
		self.modListManager = ModListManager()
		self.modListManager.reset()
		self.textRenderManager = TextRenderManager()
		self.textRenderManager.reset()
		self.modListToOpenMwConverter = ModListToOpenMwConverter()
		self.modListToOpenMwConverter.reset()

class StorageManager():
	def __init__(self):
		self.reset()

	def reset(self):
		self.homeDir = os.path.expanduser("~")
		self.openMwConfigDir = self.homeDir + "/.config/openmw"
		self.indexDir = self.openMwConfigDir + "/modindex"
		self.modIndexRawPath = self.indexDir + "/modListRaw.json"
		self.modIndexSortedPath = self.indexDir + "/modListSorted.json"
		self.modIndexEnabledPath = self.indexDir + "/modListEnabled.json"
		self.modIndexConvertedPath = self.indexDir + "/modIndexConverted.cfg"
		self.openMwConfigPath = self.openMwConfigDir + "/openmw.cfg"				# change this to not overwrite openmw.cfg
		if not os.path.exists(self.indexDir):
			os.makedirs(self.indexDir)

	def WriteToFile(self, data, file):
		with open(file, 'w') as f:
			f.write(data)

	def ReadFile(self, file):
		with open(file, 'r') as f:
			return f.read()

	def WriteToOpenMwConfig(self, newModListText, configFilePath):
		currentConfig = self.ReadFile(configFilePath)
		currentConfigLines = currentConfig.splitlines()
		parsedConfigLines = []
		for line in currentConfigLines:
			if line.startswith("content=") or line.startswith("data=") or line.startswith("fallback-archive="):
				continue
			parsedConfigLines.append(line)

		parsedConfig = "\n".join(parsedConfigLines)
		parsedConfig = "\n".join([parsedConfig, newModListText])
		self.WriteToFile(parsedConfig, configFilePath)

	def LoadStructFromJson(self, path):
		try:
			encodedStruct = globalsManager.storageManager.ReadFile(path)
			structToLoad = jsonpickle.decode(encodedStruct)
		except:
			return None
		return structToLoad

	def SaveStructAsJson(self, structToSaveRef, path):
		jsonpickle.set_encoder_options('simplejson', sort_keys=False, indent=4)
		jsonPickledModList = jsonpickle.encode(structToSaveRef)
		globalsManager.storageManager.WriteToFile(jsonPickledModList, path)

class ComparableMixin(object):
    def _compare(self, other, method):
        try:
            return method(self._cmpkey(), other._cmpkey())
        except (AttributeError, TypeError):
            # _cmpkey not implemented, or return different type,
            # so I can't compare with "other".
            return NotImplemented

    def __lt__(self, other):
        return self._compare(other, lambda s, o: s < o)

    def __le__(self, other):
        return self._compare(other, lambda s, o: s <= o)

    def __eq__(self, other):
        return self._compare(other, lambda s, o: s == o)

    def __ge__(self, other):
        return self._compare(other, lambda s, o: s >= o)

    def __gt__(self, other):
        return self._compare(other, lambda s, o: s > o)

    def __ne__(self, other):
        return self._compare(other, lambda s, o: s != o)

class Mod(ComparableMixin):
	def __init__(self, baseDir, espName = "", bsaName = "", enabled=False, textFiles = []):
		self.reset(baseDir, espName, bsaName, enabled, textFiles)

	def reset(self, baseDir, espName = "", bsaName = "", enabled=False, textFiles = []):
		self.baseDir = baseDir
		self.espName = espName
		self.bsaName = bsaName
		self.enabled = enabled
		self.textFiles = textFiles

		if espName:
			self.modName = espName
		elif bsaName:
			self.modName = bsaName
		else:
			self.modName = baseDir

	def __repr__(self):
		return "("+self.baseDir+", "+self.modName+")"
		#return self.modName

	def _cmpkey(self):
		return (self.baseDir, self.modName)

class ModListManager():
	def __init__(self):
		self.reset()

	def reset(self, modList = [], enabledMods = []):
		self.modList = modList
		self.enabledMods = enabledMods

	def LoadModIndex(self):
		pathToLoad = globalsManager.storageManager.modIndexSortedPath
		self.modList = globalsManager.storageManager.LoadStructFromJson(pathToLoad)
		if self.modList != None:
			print("successfully loaded mod index from", pathToLoad)
		else:
			print("failed to load mod index from", pathToLoad)
			self.modList = []
			self.ReIndexMods()

		pathToLoad = globalsManager.storageManager.modIndexEnabledPath
		self.enabledMods = globalsManager.storageManager.LoadStructFromJson(pathToLoad)
		if self.enabledMods == None:
			self.enabledMods = []
		print("successfully loaded enabled mod list from", pathToLoad)
		self.EnableEnabledMods()

	def EnableEnabledMods(self):
		for enabledMod in self.enabledMods:
			matchingMod = self.FindIndexedMod(enabledMod.modName, self.modList)
			if matchingMod != None:
				matchingMod.enabled = True
		self.IndexEnabledMods()

	def IndexEnabledMods(self):
		self.enabledMods = []
		for mod in self.modList:
			if mod.enabled:
				self.enabledMods.append(mod)

	def SetModEnabled(self, mod, enabled):
		mod.enabled = enabled
		if self.FindIndexedMod(mod.modName, self.enabledMods):
			if enabled == False:
				self.enabledMods.remove(mod)
		elif enabled:
			self.enabledMods.append(mod)

	def FindIndexedMod(self, modName, modList):
		for mod in modList:
			if mod.modName == modName:
				return mod
		return None

		matchingMods = [mod for mod in modList if mod.modName == modName]
		if len(matchingMods) > 0:
			return matchingMods[0]
		return None

	def IndexMods(self):
		baseDirAssetFolders = [ "bookart", "fonts", "icons", "meshes", "music", "sound", "splash", "textures", "video" ]
		for dirname, subdirs, filenames in os.walk('.', followlinks=True):
			isBaseDir=False
			isEspDir=False
			dirname = os.path.abspath(dirname)
			mod = None
			for subdir in subdirs:
				if subdir.lower() in baseDirAssetFolders:
					isBaseDir=True
					# don't crawl through asset folders
					subdirs.remove(subdir)

			modTextFiles = []
			modsToAdd = []
			for filename in filenames:
				filenameLower = filename.lower()
				if filenameLower.endswith(".esp") or filenameLower.endswith(".esm") or filenameLower.endswith(".bsa"):
					mod = Mod(dirname)
					if filenameLower.endswith(".bsa"):
						mod.reset(dirname, bsaName=filename)
					else:
						mod.reset(dirname, espName=filename)
					modsToAdd.append(mod)
					isBaseDir = True
					isEspDir = True
				elif filenameLower.endswith(".txt") or filenameLower.endswith(".etxt") or filenameLower.endswith(".html") or filenameLower.endswith(".doc") or filenameLower.endswith(".rtf"):
					modTextFiles.append(filename)

			if isBaseDir and not isEspDir:
				mod = Mod(dirname)
				mod.reset(dirname)
				modsToAdd.append(mod)

			if not mod:
				continue

			mod.textFiles = modTextFiles

			for mod in modsToAdd:
				mod.textFiles = modTextFiles
				sameNameMod = self.FindIndexedMod(mod.modName, self.modList)
				if not sameNameMod:
					continue
				for i in range(0, 100):		# ought to be enough for everybody
					tempName = mod.modName + "_"+str(i)
					if self.FindIndexedMod(tempName, self.modList):
						continue
					mod.modName = tempName
					print("duplicate mod detected, giving new name: "+str(mod))
					break

			self.modList.extend(modsToAdd)

	def ReIndexMods(self):
		modListOld = copy.copy(self.modList)
		self.modList = []
		self.IndexMods()
		self.modList = sorted(self.modList)
		modListNew = []
		if len(self.modList):
			print("successfully reindexed mods")
		else:
			print("failed to reindex mods")
		globalsManager.storageManager.SaveStructAsJson(self.modList, globalsManager.storageManager.modIndexRawPath)
		for oldMod in modListOld:
			newMod = self.FindIndexedMod(oldMod.modName, self.modList)
			if newMod == None:
				continue
			newMod.enabled = oldMod.enabled
			modListNew.append(newMod)
			self.modList.remove(newMod)
		modListNew.extend(self.modList)
		self.modList = modListNew
		self.EnableEnabledMods()

	def SaveModIndex(self, indexPath, enabledPath, convertedPath):
		globalsManager.storageManager.SaveStructAsJson(self.modList, indexPath)
		self.enabledMods = sorted(self.enabledMods)
		globalsManager.storageManager.SaveStructAsJson(self.enabledMods, enabledPath)

		convertedModList = globalsManager.modListToOpenMwConverter.Convert(self.modList)
		globalsManager.storageManager.WriteToFile(convertedModList, convertedPath)

	def ExportToOpenMw(self):
		convertedModList = globalsManager.modListToOpenMwConverter.Convert(self.modList)
		globalsManager.storageManager.WriteToOpenMwConfig(convertedModList, globalsManager.storageManager.openMwConfigPath)

class ModListToOpenMwConverter():
	def __init__(self):
		self.reset()

	def reset(self):
		None

	def CreateLauncherListName(self):
		# this is for adding our modlist to the official openmw-launcher but that doesn't happen yet
		now = datetime.datetime.now()
		prefixStr = now.strftime("%Y-%m-%d_%H:%M:%S")
		prefixStr += "/"
		return prefixStr

	def Convert(self, modListToConvert):
		baseDirList = []
		espList = []
		bsaList = []
		for mod in modListToConvert:
			if not mod.enabled:
				continue
			baseDirStr = "data=" + '"' + mod.baseDir + '"'
			baseDirList.append(baseDirStr)
			if mod.espName:
				espNameStr = "content=" + mod.espName
				espList.append(espNameStr)
			if mod.bsaName:
				bsaNameStr = "fallback-archive=" + mod.bsaName
				bsaList.append(bsaNameStr)

		convertedText = "\n".join(bsaList + baseDirList + espList)
		if convertedText:
			convertedText += "\n"
		return convertedText

class MyPad():
	def __init__(self, totalHeight=0, totalWidth=0, viewPortY=0, viewPortX=0, viewPortHeight=0, viewPortWidth=0):
		self.reset(totalHeight, totalWidth, viewPortY, viewPortX, viewPortHeight, viewPortWidth)

	def reset(self, totalHeight, totalWidth, viewPortY, viewPortX, viewPortHeight, viewPortWidth):
		self.totalHeight = totalHeight
		self.totalWidth = totalWidth
		self.viewPortY = viewPortY
		self.viewPortX = viewPortX
		self.viewPortHeight = viewPortHeight
		self.viewPortWidth = viewPortWidth
		try:
			self.pad = curses.newpad(totalHeight, totalWidth)
		except curses.error:
			pass

class TextRenderManager():
	def __init__(self):
		self.reset()

	def reset(self, window = None, inputController = None):
		self.textToRenderLock = _thread.allocate_lock()
		self.screenOpenLock = _thread.allocate_lock()
		self.windowBorderSize=1
		self.textToRender = ""
		self.SetWindow(window)
		self.SetInputController(inputController)
		self.shutDown = False

	def SetInputController(self, controller):
		self.inputController = controller
		if self.myPad != None:
			self.myPad.pad.clear()

	def SetWindow(self, window):
		self.window = window
		if window == None:
			self.maxTextDimensions = (0, 0)
			self.maxViewportDimensions = (0, 0)
			self.myPad = None
			return

		if self.windowBorderSize:
			self.window.border('*', '*', '*', '*', '*', '*', '*', '*')
		windowSize = self.window.getmaxyx()
		maxViewPortHeight = windowSize[0] - 2 * self.windowBorderSize
		maxViewPortWidth = windowSize[1] - 2 * self.windowBorderSize
		self.maxViewportDimensions = (maxViewPortHeight, maxViewPortWidth)
		self.maxTextDimensions = (maxViewPortHeight, maxViewPortWidth-1)
		self.CreateTextPad()

	def CreateTextPad(self):
		self.myPad = MyPad()
		self.myPad.reset(self.maxViewportDimensions[0],
					self.maxViewportDimensions[1],
					0, 0,
					self.maxViewportDimensions[0]+1,
					self.maxViewportDimensions[1])
		self.myPad.pad.border('.', '.', '.', '.', '.', '.', '.', '.')

	def SetTextToRender(self, textToRender):
		self.textToRenderLock.acquire()
		self.textToRender = textToRender
		self.textToRenderLock.release()

	def GetLastInput(self, window):
		prevCmd=curses.ERR
		cmd = window.getch()
		while cmd != curses.ERR:
			prevCmd = cmd
			cmd = window.getch()
		return prevCmd

	def GetDimensions(self):
		return self.maxTextDimensions

	def CloseScreen(self):
		self.shutDown = True

	def OpenScreen(self, window):
		self.screenOpenLock.acquire()
		self.SetWindow(window)
		self.window.nodelay(True)
		cmd=0
		while not self.shutDown:
			cmd = self.GetLastInput(window)
			if self.inputController != None:
				self.inputController.KeyPressed(cmd)

			time.sleep(0.1)

			if self.myPad == None:
				continue

			self.textToRenderLock.acquire()
			blankStr = ''.ljust(self.maxTextDimensions[1], " ")
			self.myPad.pad.addstr(self.maxTextDimensions[0] - 1, 0, blankStr)
			self.myPad.pad.addstr(0, 0, self.textToRender)

			#debug info
			#self.myPad.pad.addstr(0, 100, str(self.maxTextDimensions)+", "+str(self.myPad.totalHeight)+", "+str(self.myPad.totalWidth))
			#self.myPad.pad.addstr(1, 100, str(cmd))

			self.myPad.pad.refresh(self.myPad.viewPortY, self.myPad.viewPortX, 1, 1, self.myPad.viewPortHeight, self.myPad.viewPortWidth)
			self.textToRenderLock.release()
		self.screenOpenLock.release()

	def WaitScreenClosed(self):
		self.screenOpenLock.acquire()
		self.screenOpenLock.release()

class KeyboardInputController():
	def __init__():
		self.reset()

	def reset(self):
		None

	def KeyPressed(self, cmd):
		None

class ListToTextPadPresenter(KeyboardInputController):
	class RenderableIndex():
		def __init__(self):
			self.reset()
		def reset(self, itemIndex = 0, lineIndex = 0):
			self.itemIndex = itemIndex
			self.lineIndex = lineIndex

	def __init__(self):
		self.reset()

	def reset(self, parent = None, listToRender = [], textRenderManager = None):
		self.parent = parent
		self.SetListToRender(listToRender)
		self.textRenderManager = textRenderManager
		if textRenderManager != None:
			self.maxDimensions = textRenderManager.GetDimensions()
		else:
			self.maxDimensions = (0, 0)
		self.firstLineToPrintIndex = self.RenderableIndex()
		self.firstLineToPrintIndex.reset()
		self.selectedItemsList = []
		self.statusMessages = []

	def KeyPressed(self, cmd):
		textChanged=False
		if cmd == ord(' '):
			self.ToggleItemSelection()
			textChanged = True
		elif cmd == curses.KEY_DOWN:
			self.firstLineToPrintIndex.itemIndex += 1
			textChanged = True
		elif cmd == curses.KEY_UP:
			self.firstLineToPrintIndex.itemIndex -= 1
			textChanged = True
		elif cmd == curses.KEY_NPAGE:
			self.firstLineToPrintIndex.itemIndex += self.maxDimensions[0]
			textChanged = True
		elif cmd == curses.KEY_PPAGE:
			self.firstLineToPrintIndex.itemIndex -= self.maxDimensions[0]
			textChanged = True
		elif cmd == ord('q'):
			if self.parent == None:
				self.textRenderManager.CloseScreen()
				return
			self.textRenderManager.SetInputController(self.parent)
			self.parent.CreateTextFromList()
			return
		if not textChanged:
			return
		self.firstLineToPrintIndex.itemIndex = numpy.clip(self.firstLineToPrintIndex.itemIndex, 0, numpy.clip(len(self.listToRender) - 1, 0,  len(self.listToRender)))
		self.CreateTextFromList()

	def ToggleItemSelection(self):
		currentItem = self.listToRender[self.firstLineToPrintIndex.itemIndex]
		if currentItem in self.selectedItemsList:
			self.selectedItemsList.remove(currentItem)
			return
		self.selectedItemsList.append(currentItem)

	def SetListToRender(self, listToRender):
		self.listToRender = listToRender
		self.selectedItemsList = []

	def SetTextRenderManager(self, textRenderManager):
		self.textRenderManager = textRenderManager
		self.maxDimensions = textRenderManager.GetDimensions()
		self.firstLineToPrintIndex = self.RenderableIndex()
		self.firstLineToPrintIndex.reset()

	def WrapText(self, textToWrap):
		wrappedText = textwrap.wrap(textToWrap, self.maxDimensions[1], replace_whitespace=False, drop_whitespace=True, break_on_hyphens=False, break_long_words=True)
		if wrappedText == []:
			wrappedText = [""]
		return wrappedText

	def ItemToText(self, currentIndexRef):
		itemText = ""
		if currentIndexRef.itemIndex == self.firstLineToPrintIndex.itemIndex:
			itemText += "-> "
		else:
			itemText += "   "
		itemText += str(self.listToRender[currentIndexRef.itemIndex])
		return itemText
		return str(self.listToRender[currentIndexRef.itemIndex])

	def ItemToWrappedText(self, currentIndexRef):
		return self.WrapText(self.ItemToText(currentIndexRef))

	def GetItemTextAtPosition(self, currentIndexRef):
		if currentIndexRef.itemIndex < 0 or currentIndexRef.itemIndex >= len(self.listToRender):
			return None
		if currentIndexRef.lineIndex < 0:
			currentIndexRef.itemIndex -= 1
			if currentIndexRef.itemIndex < 0:
				return None
			linesToPrint = self.ItemToWrappedText(currentIndexRef)
			currentIndexRef.lineIndex = len(linesToPrint) - 1
		else:
			linesToPrint = self.ItemToWrappedText(currentIndexRef)
			if currentIndexRef.lineIndex >= len(linesToPrint):
				currentIndexRef.itemIndex += 1
				currentIndexRef.lineIndex = 0

		if currentIndexRef.itemIndex < 0 or currentIndexRef.itemIndex >= len(self.listToRender):
			return None

		return self.ItemToWrappedText(currentIndexRef)

	def GetLineTextAtPosition(self, currentIndexRef, selected=False):
		itemText = self.GetItemTextAtPosition(currentIndexRef)
		if itemText == None:
			return None

		return itemText[currentIndexRef.lineIndex]

	def CreateLinesAtPosition(self, currentIndexRef, linesToCreate):
		linesToPrint = []
		while len(linesToPrint) < linesToCreate:
			lineToPrint = self.GetLineTextAtPosition(currentIndexRef)
			if lineToPrint == None:
				break
			linesToPrint.append(lineToPrint)
			currentIndexRef.lineIndex += 1
		return linesToPrint

	def CreateLinesBeforePosition(self, currentIndexRef, linesToCreate):
		# todo: merge with afterposition function
		linesToPrint = []
		while len(linesToPrint) < linesToCreate:
			currentIndexRef.lineIndex -= 1
			lineToPrint = self.GetLineTextAtPosition(currentIndexRef)
			if lineToPrint == None:
				break
			linesToPrint.append(lineToPrint)
		return linesToPrint

	def AppendStatusMessages(self, linesToPrint, linesToCreate):
		# todo: something that can't crash so easily in small terminals
		statusLinesToAppend = []
		while linesToCreate > 0:
			if len(self.statusMessages) == 0:
				break
			statusMessage = self.statusMessages.pop()
			statusMessageLines = self.WrapText(statusMessage)
			while linesToCreate > 0 and len(statusMessageLines) > 0:
				statusMessageLine = statusMessageLines.pop()
				linesToPrint.append(statusMessageLine)
				linesToCreate -= 1

	def CreateTextFromList(self):
		linesToPrint = []

		linesToAddAfterPositionIndex = copy.copy(self.firstLineToPrintIndex)
		currentLinesToPrint = self.CreateLinesAtPosition(linesToAddAfterPositionIndex, int(self.maxDimensions[0] / 2))
		linesToPrint.extend(currentLinesToPrint)

		linesToAddBeforePositionIndex = copy.copy(self.firstLineToPrintIndex)
		currentLinesToPrint = self.CreateLinesBeforePosition(linesToAddBeforePositionIndex, self.maxDimensions[0] - len(linesToPrint) - len(self.statusMessages))
		linesToPrint.reverse()
		linesToPrint.extend(currentLinesToPrint)
		self.AppendStatusMessages(linesToPrint, self.maxDimensions[0] - len(linesToPrint))
		linesToPrint.reverse()

		currentLinesToPrint = self.CreateLinesAtPosition(linesToAddAfterPositionIndex, self.maxDimensions[0] - len(linesToPrint))
		linesToPrint.extend(currentLinesToPrint)

		textToPrint = "\n".join(linesToPrint)
		self.textRenderManager.SetTextToRender(textToPrint)

class ModDetailListToTextPadPresenter(ListToTextPadPresenter):
	def __init__(self):
		ListToTextPadPresenter.__init__(self)
	def reset (self, parent = None, modToRender = None, textRenderManager = None):
		ListToTextPadPresenter.reset(self, parent, [], textRenderManager)
		self.SetModToRender(modToRender)

	def SetModToRender(self, mod):
		self.modToRender = mod
		if(mod == None):
			self.listToRender = []
			return

		textToRender = []
		textToRender.append("esp file: " + mod.espName)
		textToRender.append("bsa file: " + mod.bsaName)
		textToRender.append("data dir: " + mod.baseDir)
		textToRender.append("")
		textToRender.append(".txt files:")
		textToRender.append("")
		for textFile in mod.textFiles:
			textFilePath = os.path.join(mod.baseDir, textFile)
			textToRender.append(textFilePath + ":")
			textFileText = ""
			try:
				textFileText = globalsManager.storageManager.ReadFile(textFilePath)
			except:
				textFileText = "error reading file - make sure it is utf-8 formatted."
			textFileTextLines = textFileText.splitlines()
			textFileTextLines.append("")
			textToRender.extend(textFileTextLines)
		self.SetListToRender(textToRender)

class ModListToTextPadPresenter(ListToTextPadPresenter):
	def __init__(self):
		ListToTextPadPresenter.__init__(self)

	def reset(self, parent = None, modListManager = None, textRenderManager = None):
		listToRender = []
		self.modListManager = modListManager
		if modListManager:
			listToRender = modListManager.modList
		ListToTextPadPresenter.reset(self, parent, listToRender, textRenderManager)

	def KeyPressed(self, cmd):
		textChanged = False
		if cmd == ord('e'):
			self.ToggleCurrentModEnabled()
			textChanged = True
		elif cmd == ord('m'):
			self.MoveSelectedItems()
			textChanged = True
		elif cmd == ord('j'):
			self.MoveCurrentItem(-1)
			textChanged = True
		elif cmd == ord('k'):
			self.MoveCurrentItem(1)
			textChanged = True
		elif cmd == ord('s'):
			self.SaveList()
			textChanged = True
		elif cmd == ord('S'):
			self.ExportList()
			textChanged = True
		elif cmd == ord('v'):
			self.ShowModReadmes()
		if textChanged:
			self.firstLineToPrintIndex.itemIndex = numpy.clip(self.firstLineToPrintIndex.itemIndex, 0, len(self.listToRender) - 1)
			self.CreateTextFromList()
			return
		ListToTextPadPresenter.KeyPressed(self, cmd)

	def ToggleCurrentModEnabled(self):
		self.modListManager.SetModEnabled(self.listToRender[self.firstLineToPrintIndex.itemIndex], not self.listToRender[self.firstLineToPrintIndex.itemIndex].enabled)

	def MoveSelectedItems(self):
		for itemToMove in self.selectedItemsList:
			self.listToRender.remove(itemToMove)
			self.listToRender.insert(self.firstLineToPrintIndex.itemIndex, itemToMove)
		self.selectedItemsList.clear()

	def MoveCurrentItem(self, direction):
		currentItem = self.listToRender[self.firstLineToPrintIndex.itemIndex]
		self.firstLineToPrintIndex.itemIndex += direction
		self.firstLineToPrintIndex.itemIndex = numpy.clip(self.firstLineToPrintIndex.itemIndex, 0, len(self.listToRender) - 1)
		self.listToRender.remove(currentItem)
		self.listToRender.insert(self.firstLineToPrintIndex.itemIndex, currentItem)

	def SaveList(self):
		self.modListManager.SaveModIndex(globalsManager.storageManager.modIndexSortedPath,
										globalsManager.storageManager.modIndexEnabledPath,
										globalsManager.storageManager.modIndexConvertedPath)
		self.statusMessages.append("Saved mod index in " + globalsManager.storageManager.modIndexSortedPath)
		self.statusMessages.append("Saved enabled mods in " + globalsManager.storageManager.modIndexEnabledPath)
		self.statusMessages.append("Saved converted modList in " + globalsManager.storageManager.modIndexConvertedPath)

	def ExportList(self):
		self.SaveList()

		globalsManager.modListManager.ExportToOpenMw()
		self.statusMessages.append("Saved converted modList in " + globalsManager.storageManager.openMwConfigPath)

	def ShowModReadmes(self):
		currentMod = self.listToRender[self.firstLineToPrintIndex.itemIndex]
		modReadmes = currentMod.textFiles
		modReadmesPresenter = ModDetailListToTextPadPresenter()
		modReadmesPresenter.reset(self, currentMod, self.textRenderManager)
		self.textRenderManager.SetInputController(modReadmesPresenter)
		modReadmesPresenter.CreateTextFromList()

	def SetListToRender(self, listToRender):
		ListToTextPadPresenter.SetListToRender(self, listToRender)

	def ItemToText(self, currentIndexRef):
		itemToPrint = self.listToRender[currentIndexRef.itemIndex]
		itemText = ""
		if currentIndexRef.itemIndex == self.firstLineToPrintIndex.itemIndex:
			itemText += "->"
		else:
			itemText += "  "
		if itemToPrint in self.selectedItemsList:
			itemText += "M"
		else:
			itemText += " "
		if itemToPrint.enabled:
			itemText += "E"
		else:
			itemText += " "
		itemText += " "
		itemText += self.listToRender[currentIndexRef.itemIndex].modName
		return itemText
		return ListToTextPadPresenter.ItemToText(self, currentIndexRef)

def PrintModListInfo():
	print(*globalsManager.modListManager.modList, sep="\n")
	print("\n".join(str(mod) for mod in globalsManager.modListManager.modList))
	print("")

def EditModIndex():
	globalsManager.textRenderManager.reset()
	_thread.start_new_thread(curses.wrapper, (globalsManager.textRenderManager.OpenScreen,))
	time.sleep(1)		# bah.
	listPresenter = ModListToTextPadPresenter()
	listPresenter.reset(None, globalsManager.modListManager, globalsManager.textRenderManager)
	globalsManager.textRenderManager.SetInputController(listPresenter)
	listPresenter.CreateTextFromList()
	globalsManager.textRenderManager.WaitScreenClosed()

def ShowHelp():
	print("Do this: load or reindex mods then edit the list then export the list then quit.")
	print("Controls for editing the mod index:")
	print("\t 'UP':\t\t select previous list item")
	print("\t 'DOWN':\t select next list item")
	print("\t 'PGUP':\t scroll up one page")
	print("\t 'PGDOWN':\t scroll down one page")
	print ("\t 'e':\t\t enable/disable selected mod (symbolized with 'E' tag)")
	print("\t <space>:\t mark selected mod (symbolized with 'M' tag)")
	print("\t 'm':\t\t move marked mods to selected position")
	print("\t 'j':\t\t move selected mod up in list")
	print("\t 'k':\t\t move selected mod down in list")
	print("\t 's':\t\t save mod list")
	print("\t 'S':\t\t export mod list to openmw.cfg")
	print("\t 'v':\t\t view details and readme files of selected mod. only works with some readme files.")
	print("\t 'q':\t\t return to previous menu")
	print("")
	print("Warning: Exporting to openmw.cfg overwrites your current data= and content= mod lines")
	print("")
	print("Paths:")
	print("\t home:", globalsManager.storageManager.homeDir)
	print("\t modindexer config:", globalsManager.storageManager.indexDir)
	print("\t raw mod list:", globalsManager.storageManager.modIndexRawPath)
	print("\t sorted mod list:", globalsManager.storageManager.modIndexSortedPath)
	print("\t converted for openmw mod list:", globalsManager.storageManager.modIndexConvertedPath)
	print("\t openmw.cfg:", globalsManager.storageManager.openMwConfigPath)
	input("Press enter to return")

def RunOpenMw():
	print("Launching OpenMW...")
	# copypasted from stackoverflow
	subprocess.Popen(['nohup', 'openmw'],
                 stdout=open('/dev/null', 'w'),
                 stderr=open('/tmp/logfile.log', 'a'),
                 preexec_fn=os.setpgrp
                 )

def ShutDown():
	exit()

def ShowMainMenu():
	commandDelegates = {0 : globalsManager.modListManager.LoadModIndex,
						1 : globalsManager.modListManager.ReIndexMods,
						2 : PrintModListInfo,
						3 : EditModIndex,
						4 : globalsManager.modListManager.ExportToOpenMw,
						5 : ShowHelp,
						6 : RunOpenMw,
						7 : ShutDown
					}

	commandList = ["Load mod index", "Refresh mod index", "Print mod index", "Edit mod index", "Export to openmw.cfg", "Help", "Run OpenMW", "Quit"]
	while True:
		print("")
		print("What do you want?")
		for i in range(0, len(commandList)):
			print("\t", i, "-", str(commandList[i]))
		selectedCommand = int(input("\t Enter number: "))
		print("")
		if selectedCommand >= len(commandList):
			continue
		commandDelegates[selectedCommand]()

globalsManager = GlobalsManager()
globalsManager.reset()

globalsManager.modListManager.LoadModIndex()
ShowMainMenu()








