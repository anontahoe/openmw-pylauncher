OpenMW-PyLauncher
=================

Lets you easily index lots of mod folders, enable and disable mods and shuffle them around, including changing the load order of data directories (e.g. for texture and sound replacement mods).  
This script is in early alpha stage because as soon as it worked as intended i started playing.  
That means you really must read this entire readme file, but you're probably used to that.  

#### What it does / Notes / Pitfalls / Bugs
*	The script crawls recursively through subdirectories of the current working directory and tries to detect all mod folders.  
*	Mod folders are directories containing .esm or .esp files or containing certain subdirectories like "meshes", "textures", "sound", etc.  
*	It also tries to find readme files based on a hardcoded list of file extensions and it only works if all characters in the readme are utf-8 so it's broken as fuck.  
*	It does not yet display any mod metadata or take dependencies into account but i checked and the esp format is easy to read so i might add that in the future.  
*	If you export your list it overwrites your openmw.cfg, removing all data= and content= lines and adding only those mods that you enabled.  
*	The mod index and list of enabled mods is stored in ~/.config/openmw/modindex/, any changes to data and content in openmw.cfg will be ignored and overwritten the next time you export your list.  
*	Some mods (in my case only some herbalism mod) need you to add fallback-archive=Something.bsa to openmw.cfg by hand. You still need to do that. I suppose it could be automated though...  

#### How to download
*	https: git clone https://gitlab.com/anontahoe/openmw-pylauncher.git  
*	ssh: see [here][] for instructions  

#### Dependencies
*	Python 3 (tested on 3.4)
*	Various python modules... This is what I import: os, time, jsonpickle, curses, sys, textwrap, numpy, _thread, copy, datetime, subprocess  
	Just install whatever throws an error.


#### How to use
*	Because OpenMW-PyLauncher replaces all content= and data= lines in the openmw.cfg you will have to treat the morrowind data folder (where Morrowind.esm is) like any other mod.  
	Move or symlink it into the folder where all your other mods are located.  
*	Move openmw-pylauncher.py to /bin/, /usr/games/bin/ or wherever and make it executable (chmod +x)  
*	cd to the folder in which you keep all your Morrowind mods and run openmw-pylauncher.py  
	Note: OpenMW-PyLauncher doesn't work if it is not started from your mod directory.
*	Select "Refresh index" if you added, removed or renamed any folders (your settings will be preserved)  
	This is done automatically when you run OpenMW-PyLauncher for the very first time, so you can skip it.
*	Select "Edit mod index" to enable/disable or shuffle around indexed mods  
*	Select "Help" to get an explanation of keyboard controls. Look at them or you won't be able to "Edit mod index". I hope you like Dwarf Fortress.  
*	When you edit the index, remember to move Morrowind/Tribunal/Bloodmoon.esm to the top of the index and enable them.  

If you have any questions you can send an email to anontahoe(at)anonmail.pwnz.org and I'll be happy to help.  

[here]: https://meshnet.pwnz.org/tahoe/deploy-key.html
